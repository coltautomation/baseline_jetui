package com.client.cpq.uitests.admin;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.colt.common.utils.dataminer;
import com.colt.common.utils.dateTimeUtil;
import com.colt.common.utils.dataminer;

public class SampleTest {
	
	public static String fnGetCurrentTime() {
		
//		Initializing the Time format
		DateFormat timeFormat = null;
		Date time = null;
		timeFormat = new SimpleDateFormat("HH:mm:ss.SSS");
		
		time = new Date();
		String CurrentTime = timeFormat.format(time);
		System.out.println(CurrentTime);
		return CurrentTime;
		
	}
	
	public static String fnGetElapsedTime(String StartTime, String EndTime) throws ParseException {
		
//		Initializing the Time format
		DateFormat timeFormat = null;
		timeFormat = new SimpleDateFormat("HH:mm:ss.SSS");
		
		Date date1 = timeFormat.parse(StartTime);
	    Date date2 = timeFormat.parse(EndTime);
	    float difference = (date2.getTime() - date1.getTime());
	    BigDecimal bigVal = BigDecimal.valueOf(difference).divide(BigDecimal.valueOf(1000),3,RoundingMode.HALF_UP);
	    String retVal = bigVal.toString();
	    return retVal;
		
	}

	public static void main(String[] args) throws IOException, InterruptedException, ParseException {
		
		String tfile_name =  System.getProperty("user.dir")+"\\TestData\\CPQ_Baseline_Timings.xlsx";
		String UI_Type = "TS_03_Medium_Scenario";
		String Prod_Code = "EL";
		int i = 2;
		String Sales_Config_TimeDiff = "23.876";
		String scol = Prod_Code+"EL_CST_Confg_LI4";
		dataminer.fnsetTransactionValue(tfile_name, UI_Type, Prod_Code+"_CST_Confg_LI5" , "Attempt_"+Integer.toString(i), Sales_Config_TimeDiff);
		System.out.println("Value Parsing is done");
//		String Time1 = "12.345"; String Time2 = "32.876";
//	
////		double svalue = Double.parseDouble(Time1) + Double.parseDouble(Time2);
//		String s = Double.toString(Double.parseDouble(Time1) + Double.parseDouble(Time2));
//		
//		System.out.println("SValue is "+s);
//		
//		
//		//declaring the source values
//		String file_name =  System.getProperty("user.dir")+"\\TestData\\Transactions_Capture.xlsx";
//		String Sheet_Name = "Scenario_1";
//		
//		String aPacTransaction = "EL_26|EL_27:EL_29|EL_30:EL_32|EL_33:EL_35|EL_36:EL_38|EL_39";
//		String[] pacTransaction = aPacTransaction.split("\\:");
//		System.out.println("1st array is "+pacTransaction[0]);
		
//		
////		Entering the Values to the Data sheet
//		dataminer.fnsetTransactionValue(file_name, Sheet_Name, "TR_02", "End_Time", EndTime);
//		dataminer.fnsetTransactionValue(file_name, Sheet_Name, "TR_02", "Start_Time", StartTime);
//		dataminer.fnsetTransactionValue(file_name, Sheet_Name, "TR_02", "Elapsed_Time", TimeDiff);

		
	}

}
