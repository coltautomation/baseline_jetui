package com.client.cpq.uitests.pageobjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.colt.common.utils.DriverManagerUtil;


public class CPQ_Objects {
	
	//C4C Login Page
	@FindBy(xpath = "//input[@id='userId']") public WebElement userNameTxb;	//Username
	@FindBy(xpath = "//input[@name='Ecom_Password']") public WebElement passWordTxb;	//Password
//	@FindBy(xpath = "//button[text()='Login ']") public WebElement loginBtn;	//Login Button
	@FindBy(xpath = "//button[@type='submit']") public WebElement loginBtn;	//Login Button
	@FindBy(xpath = "//button[contains(text(),'Login Back To Colt Online')]") public WebElement LoginBackToColtOnlineBtn;	//LoginBackToColtOnlineBtn
	@FindBy(xpath = "//button[@title='Click to Personalize / Adapt']") public WebElement verifyEditLnk; //Edit Link in Home page
	
//	C4C Logout Page
	@FindBy(xpath = "//div[contains(@id,('header-user-image'))]") public WebElement c4cUserImgLnk;
	@FindBy(xpath = "//bdi[text()='Sign Out']") public WebElement c4cSignOutBtn;
	@FindBy(xpath = "//button//bdi[text()='Yes']") public WebElement c4cAcceptSignoutBtn;
	@FindBy(xpath = "//button[@id='LOGIN_LINK']") public WebElement c4cLogOnBtn;

//	CPQ Login page
	@FindBy(xpath = "//input[@id='username']") public WebElement cpqUserNameTxb;
	@FindBy(xpath = "//label[text()='Password:']//following-sibling::input") public WebElement cpqPassWordTxb;
	@FindBy(xpath = "//a[text()='Log in']") public WebElement cpqLoginBtn;	

//	C4CMain Page
	@FindBy(xpath = "//span[@title='Customers']") public WebElement customerLst;	//Customer dropdown
	@FindBy(xpath = "//a[@title='Accounts']") public WebElement accountsTab;	//Accounts Option
	@FindBy(xpath = "//*[contains(@title,'Please wait')]") public WebElement c4cLoader;	//c4c Loader
	@FindBy(xpath = "//span[@title='Sales']") public WebElement salesLst;
	@FindBy(xpath = "//a[@title='Quotes']") public WebElement quotesTab;
	@FindBy(xpath = "//oj-progress[@title='Processing']") public WebElement prodConfigLoadProcessIcn;
	@FindBy(xpath = "//oj-progress[@title='Completed']") public WebElement prodConfigLoadCompleteIcn;
	@FindBy(xpath = "//div[contains(text(),'Results expected in less than')]") public WebElement prodConfigLoadingIcn;
	@FindBy(xpath = "//span[text()='Quote']//parent::a//parent::li//following-sibling::li[1]") public WebElement plTab;
	
//	Search Quote and Opportunities
	@FindBy(xpath = "//span[contains(@id,'variantManagement-trigger-img')]") public WebElement selectViewDn;	//Select View Dropdown
	@FindBy(xpath = "//li[contains(text(),'All')]") public WebElement allLst;		//All Value in dropdown
	@FindBy(xpath = "//div[@aria-label='Select all rows'][1]") public WebElement defaultCkb;		//to verify Check box is enbled
	@FindBy(xpath = "//span[contains(@id,'searchButton-img')]") public WebElement searchIcon;	//Search Icon
	@FindBy(xpath = "//input[@type='search']") public WebElement searchTxb;	//Search Input Box
	@FindBy(xpath = "//div[@title='Search']") public WebElement searchBtn;	//Search Button or Icon
	
//	Oppurtunity Screen
	@FindBy(xpath = "//div[text()='Opportunities']") public WebElement OpportunitiesLnk;	//Oppurtunity Link
	@FindBy(xpath = "//bdi[text()='New']") public WebElement opportunityNewBtn;	//New Button
	@FindBy(xpath = "//span[@title='Name']//following-sibling::div//..//input") public WebElement NameTxb;	//New Button
	@FindBy(xpath = "//span[@title='Offer Type']//following-sibling::div//child::span") public WebElement OfferTypeLst;	//New Button
	@FindBy(xpath = "//span[@title='Campaign']//following-sibling::div//child::input") public WebElement CampaignTxb;
	@FindBy(xpath = "//span[@title='Primary Programme']//following-sibling::div//child::span") public WebElement PrimaryProgramLst;
	@FindBy(xpath = "//span[@title='Primary Attribute']//following-sibling::div//child::span") public WebElement PrimaryAttributeLst;
	@FindBy(xpath = "//span[@title='Opportunity Currency']//following-sibling::div//child::span") public WebElement OpportunityCurencyLst;
	@FindBy(xpath = "//div[contains(@id,'button')]//button[@title='More']") public WebElement saveMoreLnk;
	@FindBy(xpath = "//bdi[text()='Save and Open']") public WebElement saveOpenLnk;
	@FindBy(xpath = "//div[@data-help-id='messageBar-headerBar']//..//span[contains(@data-help-id,'messageBar')]") public WebElement headerBar;
	@FindBy(xpath = "//span[@title='Opportunity ID']//following-sibling::div//child::span") public WebElement oppurtunityIDBx;
	@FindBy(xpath = "//bdi[text()='More']//parent::span//parent::span//parent::button") public WebElement moreLnk;
	@FindBy(xpath = "//button[@title='Expand']") public WebElement expandLnk;
	@FindBy(xpath = "//button[@title='Collapse']") public WebElement collapseLnk;
	@FindBy(xpath = "//button[@title='Edit']") public WebElement editOpportunityLnk;
	@FindBy(xpath = "//span[@title='Sales Unit']//following-sibling::div//..//input") public WebElement salesUnitTxb;	
	@FindBy(xpath = "//span[@title='Legal complexity']//following-sibling::div//child::span") public WebElement legalComplexityLnk;
	@FindBy(xpath = "//span[@title='Technical complexity']//following-sibling::div//child::span") public WebElement techComplexityLnk;
	@FindBy(xpath = "//descendant::bdi[text()='Save'][2]") public WebElement saveBtn;
	
//	SE Engagement Screen in C4C
	@FindBy(xpath = "//div[text()='Engagement']") public WebElement EngagementLnk;
	@FindBy(xpath = "//button[@title='Edit']") public WebElement editEngagementLnk;
	@FindBy(xpath = "//div[@title='Request SE']") public WebElement engageSECbx;
	@FindBy(xpath = "//div[@title='Consultant']") public WebElement consultantCbx;
	@FindBy(xpath = "//span[contains(@title,'SE Engagement')]//following-sibling::div//child::span") public WebElement seEngagementLst;
	@FindBy(xpath = "//ul[@role='listbox']") public WebElement defaultListElem;
	@FindBy(xpath = "//bdi[text()='Save']") public WebElement saveC4CBtn;
	
//	C4C Product Screen
	@FindBy(xpath = "//div[text()='Products']") public WebElement productsLnk;
	@FindBy(xpath = "//bdi[text()='Product']//parent::span//following-sibling::div//div//div//input") public WebElement productsTxb;
	@FindBy(xpath = "//ul[contains(@id,'inputField-popup-list-listUl')]") public WebElement productsSubLst;
	
	@FindBy(xpath = "//bdi[text()='Contract Length In Months']//parent::span//following-sibling::div//div//div//input") public WebElement contContractsLenMnthTxb;
	@FindBy(xpath = "//bdi[text()='Bandwidth']//parent::span//following-sibling::div//div//div//input") public WebElement containerbandWidthLst;
	@FindBy(xpath = "(//bdi[text()='Add']//parent::span//parent::span//parent::button)[1]") public WebElement containerAddBtn;
	@FindBy(xpath = "//bdi[text()='Container - New Business']//parent::span//parent::span//parent::button") public WebElement addNewcontainerBtn;

//	Container Upload Files
	@FindBy(xpath = "//span[contains(@onclick,'listPage_upload_quote_line')]") public WebElement containerUploadBtn;
	@FindBy(xpath = "//input[@value='internal']//parent::div") public WebElement contUpldInternalRdb;
	@FindBy(xpath = "//input[@value='customer_facing']//parent::div") public WebElement contUpldcustomerRdb;
	
	@FindBy(xpath = "//h4[text()='Upload Document(s)']") public WebElement contUpldDocTxt;
	@FindBy(xpath = "//div[@class='modal-body']//select[@id='document_type']") public WebElement contSelectDocDrp;
	@FindBy(xpath = "//div[@class='modal-body']//b[text()='Choose File:']//parent::div//following-sibling::div//input") public WebElement contchooseFile;
	@FindBy(xpath = "//div[@class='modal-footer']//button[@id='upload_button']") public WebElement contUploadFile;
	@FindBy(xpath = "//div[@id='oj-select-choice-whyIsContainerJourneySelected_t']//a") public WebElement contwhyisContainerFileLst;
	

//	Quotes Screen
	@FindBy(xpath = "//div[text()='Quotes']") public WebElement quotesLnk;
	@FindBy(xpath = "//bdi[text()='Add']//parent::span//parent::span//parent::button") public WebElement addQuoteBtn;
	@FindBy(xpath = "//bdi[contains(text(),'Action')]") public WebElement actionBtn;	//New Button
	@FindBy(xpath = "//bdi[text()='Edit']") public WebElement EditBtn;	//New Button
	@FindBy(xpath = "//bdi[text()='View']") public WebElement ViewBtn;	//New Button
	@FindBy(xpath = "//a[@id='save']") public WebElement save4cBtn;	//New Button
	@FindBy(xpath = "//oj-button[@name='update']") public WebElement update4cBtn;	//Update Button
	@FindBy(xpath = "//bdi[text()='Copy']//parent::span") public WebElement copyQuoteC4CBtn;
	@FindBy(xpath = "//bdi[text()='Proceed']//parent::span") public WebElement proceedC4CBtn;
	
// Quotes MultiPuropse
	@FindBy(xpath = "//bdi[text()='Quote Currency']//parent::span//following-sibling::div//input") public WebElement quoteCurrencyDBtn;
	@FindBy(xpath = "//bdi[text()='Legal Complexity']//parent::span//following-sibling::div//input") public WebElement legalComplexityDBtn;
	@FindBy(xpath = "//bdi[text()='Technical Complexity']//parent::span//following-sibling::div//input") public WebElement techicalComplexityDBtn;
	@FindBy(xpath = "//bdi[text()='Proceed']/parent::*/parent::*/parent::*") public WebElement proceedBtn;
	@FindBy(xpath = "//div[contains(@id,'ojChoiceId_quoteType_t')]//a") public WebElement quoteTypeBtn;
	@FindBy(xpath = "//li//div[text()='Multi-quote']") public WebElement multiTypeBtn;
	@FindBy(xpath = "//input[@value='Multi-Quote']//parent::span") public WebElement multiQuoteCbx;
	@FindBy(xpath = "//span[text()='Download Quote']") public WebElement downloadQuote;
	@FindBy(xpath = "//span[text()='Download Quote Excel-External']") public WebElement downloadQuoteExternal;
	@FindBy(xpath = "//span[text()='Proceed to Quote']") public WebElement procedToQuoteBtn;
	@FindBy(xpath = "//label[text()='Account Details']") public WebElement accountDetailsLnk;
	@FindBy(xpath = "//input[@id='oCN_t']") public WebElement ocnTxt;
	@FindBy(xpath = "//input[@id='oCN_t' and @readonly='readonly']") public WebElement ocnReadTxt;
	@FindBy(xpath = "//div[@id='oj-select-choice-pricingSegment_t']") public WebElement priceSegmentDrp;
	@FindBy(xpath = "//td[@id='cancel_configuration']") public WebElement cancelConfiguration;
	
//	CPQ Home Page
	@FindBy(xpath = "//span[text()='Return to C4C']") public WebElement returnToC4CBtn;
	@FindBy(xpath = "//input[contains(@id,'quoteName')]") public WebElement quoteNameTxb;
	@FindBy(xpath = "//input[contains(@id,'quoteType_t')]") public WebElement quoteTypeElem;
//	@FindBy(xpath = "//div[contains(@id,'oj-select-choice-quoteType_t')]") public WebElement quoteTypeElem;
	@FindBy(xpath = "//descendant::input[contains(@id,'quoteID')]") public WebElement quoteIDElem;
	@FindBy(xpath = "//descendant::input[contains(@id,'status_t')]") public WebElement quoteStageElem;
	@FindBy(xpath = "//descendant::input[contains(@id,'statusNew')]") public WebElement quoteStatusElem;
	@FindBy(xpath = "//span[text()='SE Engagement']") public WebElement seRevLnk;
	@FindBy(xpath = "//div[@id='lockCreateScreen' and not(@style='display: none;')]") public WebElement cpqLoadMask;
	@FindBy(xpath = "//button[@name='configuration_complete,_send_to_sales']") public WebElement confCompleteSendSalesBtn;
	
//	Adding Product
	@FindBy(xpath = "//span[text()='Add Product']") public WebElement addProductBtn;
	@FindBy(xpath = "//a[text()='CPE Solutions']") public WebElement cpeSolutionLnk;
	@FindBy(xpath = "//a[text()='CPE Solutions Service']") public WebElement cpeServiceLnk;
	@FindBy(xpath = "//a[text()='CPE Solutions Site']") public WebElement cpeSiteLnk;
	@FindBy(xpath = "//a[text()='Ethernet']") public WebElement ethernetLnk;
	@FindBy(xpath = "//a[text()='Ethernet Line']") public WebElement ethernetLineLnk;
	@FindBy(xpath = "//a[text()='Ethernet Hub']") public WebElement ethernetHubLnk;
	@FindBy(xpath = "//a[text()='Ethernet Spoke']") public WebElement ethernetSpokeLnk;
	@FindBy(xpath = "//a[text()='Optical']") public WebElement opticalLnk;
	@FindBy(xpath = "//a[text()='Wave']") public WebElement waveLnk;
	@FindBy(xpath = "//a[text()='IP Access']") public WebElement IPAccessLnk;
	@FindBy(xpath = "//a[text()='Colt IP Access']") public WebElement ColtIpAccessLnk;
	@FindBy(xpath = "//a[text()='Colt IP Domain']") public WebElement ColtIpDomainLnk;
	@FindBy(xpath = "//a[text()='Colt IP Guardian']") public WebElement ColtIpGuardianLnk;
	@FindBy(xpath = "//a[text()='Colt Managed Virtual Firewall']") public WebElement ColtManagedVirtualFirewallLnk;
	@FindBy(xpath = "//a[text()='Colt Managed Dedicated Firewall']") public WebElement ColtManagedDedicatedFirewallLnk;
	@FindBy(xpath = "//a[text()='Start']") public WebElement ColtIpAccessStartLnk;
	@FindBy(xpath = "//a[text()='Professional Services']") public WebElement professionalServicesLnk;
	@FindBy(xpath = "//a[text()='Professional Services - Data']") public WebElement professionalServicesDataLnk;
	@FindBy(xpath = "//a[text()='Professional Services - Voice']") public WebElement professionalServicesVoiceLnk;
	@FindBy(xpath = "//a[text()='Data - Consultancy']") public WebElement dataConsultancyLnk;
	@FindBy(xpath = "//a[text()='Data - Project Management']") public WebElement dataProjectManagement;
	@FindBy(xpath = "//a[text()='Data - Service Management']") public WebElement dataServiceManagement;
	@FindBy(xpath = "//a[text()='Voice - Consultancy']") public WebElement voiceConsultancyLnk;
	@FindBy(xpath = "//a[text()='Voice - Project Management']") public WebElement voiceProjectManagement;
	@FindBy(xpath = "//a[text()='Voice - Service Management']") public WebElement voiceServiceManagement;
	@FindBy(xpath = "//a[text()='Data']") public WebElement dataLink;
	@FindBy(xpath = "//a[text()='VPN']") public WebElement vpnLink;
	@FindBy(xpath = "//a[text()='VPN Data']") public WebElement vpnDataLink;
	@FindBy(xpath = "//a[text()='VPN Network']") public WebElement vpnNetworkLink;
	
//	Delete the Product
	@FindBy(xpath = "//oj-button[@title='Remove']") public WebElement deleteProductBtn;
	@FindBy(xpath = "//div[@class='oj-dialog-footer']//..//span[text()='OK']") public WebElement okdialogButton;
	
//	IPAddon Products
	@FindBy(xpath = "//div[contains(@id,'countryIPD')]//following-sibling::a") public WebElement ipDomainCountryLst;
	@FindBy(xpath = "descendant::ul[@role='listbox'][1]") public WebElement ipAddOnCountrySubLst;	
	@FindBy(xpath = "//div[contains(@id,'currencyDisplay')]//following-sibling::a") public WebElement ipDomainCurrencyLst;
	@FindBy(xpath = "//ul[contains(@id,'currencyDisplay')]") public WebElement ipDomainCurrencySubLst;
	@FindBy(xpath = "//input[contains(@id,'iPAccessServiceReference')]") public WebElement ipAccessServiceReferenceTxb;
	@FindBy(xpath = "descendant::span[text()='Select...'][1]") public WebElement clickDefaultCellIpDomainTble;	
	@FindBy(xpath = "//div[contains(@id,'domainOrderTypeCountrySpecific')]//following-sibling::a") public WebElement domainTypeGenericLst;
	@FindBy(xpath = "//ul[contains(@id,'domainOrderTypeCountrySpecific')]") public WebElement domainTypeGenericSubLst;
	@FindBy(xpath = "//div[contains(@id,'topLevelDomainGenericPickList')]//following-sibling::a") public WebElement topLevelDomainLst;
	@FindBy(xpath = "//input[contains(@id,'domainName')]") public WebElement ipDomainNameTxb;
	@FindBy(xpath = "//select[@name='serviceBandwidthIpGuardian']") public WebElement ipGuardianBandwidthLst;
	@FindBy(xpath = "//div[contains(@id,'customerType')]//following-sibling::a") public WebElement customerTypeLst;
	@FindBy(xpath = "//div[contains(@id,'countryIPG')]//following-sibling::a") public WebElement ipGuardianCountryLst;
	@FindBy(xpath = "//input[contains(@id,'iPAccessServiceReferenceipguardian')]") public WebElement ipGuardianServiceReferenceTxb;
	@FindBy(xpath = "//div[contains(@id,'countryIPMVF')]//following-sibling::a") public WebElement mvfCountryLst;
	@FindBy(xpath = "//select[@name='serviceBandwidthIPMVF']") public WebElement mvfBandwidthLst;
	@FindBy(xpath = "//input[contains(@id,'iPAccessServiceReferenceIPMVF')]") public WebElement mvfServiceReferenceTxb;
	@FindBy(xpath = "//div[contains(@id,'managedDedicatedFirewallCountryAEnd')]//following-sibling::a") public WebElement mdfCountryLst;
	@FindBy(xpath = "//select[@name='serviceBandwidthIPMDF']") public WebElement mdfBandwidthLst;
	@FindBy(xpath = "//input[contains(@id,'iPAccessServiceReferenceIPMDF')]") public WebElement mdfServiceReferenceTxb;	
	
	
//	Bulk Upload Products
	@FindBy(xpath = "//span[text()='Bulk Upload']") public WebElement bulkUploadLnk;
	@FindBy(xpath = "//input[@value='Add New Line Items to Quote']") public WebElement bulkAddNewLineBtn;
	@FindBy(xpath = "//input[@value='Upload New File']") public WebElement bulkUploadNewFlieBtn;
	@FindBy(xpath = "//input[@id='fileInput']") public WebElement bulkfileInputBtn;
	@FindBy(xpath = "//input[@value='Upload']") public WebElement bulkUploadBtn;
	@FindBy(xpath = "//div[text()='Please wait,File Upload in Progress....']") public WebElement bulkPleaseWaitText;
	@FindBy(xpath = "//span[text()='Uploaded']") public WebElement bulkUploadedTxt;
	@FindBy(xpath = "//span[text()='Details']") public WebElement bulkDetailsTxt;
	@FindBy(xpath = "//button[text()='Refresh']") public WebElement bulkRefreshBtn;
	@FindBy(xpath = "//button[text()=' Check Connectivity']") public WebElement bulkCheckConnectivityBtn;
	@FindBy(xpath = "//span[contains(text()=' Connectivity Check in')]") public WebElement bulkConnectivityTxt;
	@FindBy(xpath = "//input[@id='fromAend']//preceding-sibling::span[.='ONNET'  and not(contains(@disabled,'disabled'))]") public WebElement bulkOnnetACheckRdb;
	@FindBy(xpath = "//input[@id='fromBend']//preceding-sibling::span[.='ONNET'  and not(contains(@disabled,'disabled'))]") public WebElement bulkOnnetBCheckRdb;
	@FindBy(xpath = "//span[text()='Select All']//preceding-sibling::input") public WebElement bulkSelectAllCbx;
	@FindBy(xpath = "//button[text()='Add To Quote']") public WebElement bulkAddToQuoteBtn;
	@FindBy(xpath = "//button[text()='Pending']") public WebElement bulkPendingBtn;
	@FindBy(xpath = "//button[text()='Continue To Quote']") public WebElement bulkContinueToQuoteBtn;
	
//	Professional Services Configuration
	@FindBy(xpath = "//div[contains(@id,'serviceType')]//following-sibling::a") public WebElement serviceTypeLst;
	@FindBy(xpath = "//input[contains(@id,'effortEstimationInManDays')]") public WebElement effortEstimationManDaysTxb;	
	@FindBy(xpath = "//input[contains(@id,'grossNonRecurringChargesNRC')]") public WebElement grosssNRCElem;
	@FindBy(xpath = "//div[contains(@id,'packageType')]//following-sibling::a") public WebElement packageTypeLst;
	@FindBy(xpath = "//input[contains(@id,'resourceRequired0')]") public WebElement resourceRequired;
	@FindBy(xpath = "//div[contains(@id,'contractTermInYears')]//following-sibling::a") public WebElement contractTermInYears;
	
//	CPE Solutions Service Cofiguration pagein CPQ
	@FindBy(xpath = "//select[@id='networkTopology']") public WebElement networkTopologyLst;
	@FindBy(xpath = "//label[@id='networkTopology_label']") public WebElement networkTopologyLabel;
	@FindBy(xpath = "//span[text()='CPE Site Configuration']") public WebElement cpeSiteConfigurationLnk;
	@FindBy(xpath = "//div[contains(@id,'cPESolutionService')]//following-sibling::a") public WebElement cpeSolutionServiceLst;
	@FindBy(xpath = "//ul[contains(@id,'cPESolutionService')]") public WebElement cpeSolutionServiceSubLst;
	@FindBy(xpath = "//label[text()='Select' and @for='selectServiceRef_0']") public WebElement defSelectServiceBtn;
	@FindBy(xpath = "//span[text()='RELATED NETWORK PRODUCT']") public WebElement relatedNetworkProductLnk;
	@FindBy(xpath = "//div[contains(@id,'relatedNetworkProductReferenceType')]//following-sibling::a") public WebElement relatedNetworkProductLst;
	@FindBy(xpath = "//ul[contains(@id,'relatedNetworkProductReferenceType')]") public WebElement relatedNetworkProductSubLst;
	@FindBy(xpath = "//span[text()='CPE SITE ADDRESS']") public WebElement cpeSiteAddressLnk;
	@FindBy(xpath = "//input[contains(@id,'relatedCPESolutionServiceReference')]") public WebElement relatedServiceReferenceTxb;
	@FindBy(xpath = "//input[contains(@id,'relatedNetworkProductReference')]") public WebElement relatedNetworkReferenceTxb;
	@FindBy(xpath = "//div[contains(@id,'hubType')]//following-sibling::a") public WebElement hubTypeLst;
	@FindBy(xpath = "//ul[contains(@id,'hubType')]") public WebElement hubTypeSubLst;
	@FindBy(xpath = "//input[contains(@id,'hubReferenceID')]") public WebElement hubReferenceTxb;
	@FindBy(xpath = "//input[contains(@id,'siteAddressHubExisting')]") public WebElement hubAddressTxb;
	@FindBy(xpath = "//descendant::img[@title='Search for an Address'][1]") public WebElement hubSearchImgLnk;
	@FindBy(xpath = "//descendant::img[@title='Search for an Address'][2]") public WebElement spokeSearchImgLnk;
	
//	Vpn Network
	@FindBy(xpath = "//div[@id='oj-select-choice-networkType']//a") public WebElement vpnSelectChoiceNetworkLnk;
	@FindBy(xpath = "//oj-progress[contains(@title,'Processing')]") public WebElement vpnLoadBarPorcess;
	@FindBy(xpath = "//span[text()='ADD']") public WebElement vpnAddBTN;
	@FindBy(xpath = "(//span[@class='oj-treeview-item-text'])[2]") public WebElement vpnTreeLink;
	@FindBy(xpath = "//span[text()='Add-Ons']") public WebElement vpnAddOnLink;
	@FindBy(xpath = "//input[@name='outsideBusinessInstallationHoursVPN']") public WebElement vpnOusideBusinessCbx;
	@FindBy(xpath = "//input[@name='longLiningVPN']") public WebElement vpnLongLiningCbx;
	@FindBy(xpath = "(//span[@class='oj-treeview-item-text'])[1]") public WebElement vpnNetworkLeftLink;
	
//	Proactive Contact
	@FindBy(xpath = "//oj-switch[contains(@id,'proactiveManagementContact')]") public WebElement proactiveContactCbx;
	@FindBy(xpath = "//div[contains(@id,'-title_qto')]//following-sibling::a") public WebElement pacTitleLst;
	@FindBy(xpath = "//ul[contains(@id,'title_qto')]") public WebElement pacTitleSubLst;
	@FindBy(xpath = "//input[contains(@id,'firstName_qto')]") public WebElement pacFirstNameTxb;
	@FindBy(xpath = "//input[contains(@id,'lastName_Qto')]") public WebElement pacLastNameTxb;
	@FindBy(xpath = "//input[contains(@id,'contactNumber_Qto')]") public WebElement pacContactNumberTxb;
	@FindBy(xpath = "//input[contains(@id,'mobileNumber_qto')]") public WebElement pacMobileNumberTxb;
	@FindBy(xpath = "//input[contains(@id,'email_Qto')]") public WebElement pacEmailTxb;
	@FindBy(xpath = "//div[contains(@id,'correspondanceLanguage')]//following-sibling::a") public WebElement pacLanguageLst;
	@FindBy(xpath = "//ul[contains(@id,'correspondanceLanguage')]") public WebElement pacLanguageSubLst;
	@FindBy(xpath = "//div[contains(@id,'preferredContactMethod')]//following-sibling::a") public WebElement pacPreferredContactLst;
	@FindBy(xpath = "//ul[contains(@id,'preferredContactMethod')]") public WebElement pacPreferredContactSubLst;
	
//	Vpn Address
	@FindBy(xpath = "//input[contains(@id,'siteAddressSearchVPN')]") public WebElement vpnSiteAddressTxb;
	@FindBy(xpath = "//div[@id='searchButton']//img") public WebElement vpnSiteSearchImg;
	@FindBy(xpath = "//span[text()='Next']") public WebElement vpnNextButton;
	@FindBy(xpath = "//span[text()='Site Configuration']") public WebElement vpnsiteConfiguartionLink;
	@FindBy(xpath = "//div[@id='oj-select-choice-linkTypeVPN']//a") public WebElement vpnlinkTypeLink;
	
//	Vpn Site configuration
	@FindBy(xpath = "//div[@id='oj-select-choice-contractTermSiteDetails']//a") public WebElement vpnContractYearLnk;
	@FindBy(xpath = "//div[@id='oj-select-choice-serviceBandwidthSiteDetails']//a") public WebElement vpnServiceBandLnk;
	@FindBy(xpath = "//div[@id='oj-select-choice-resilienceSiteDetails']//a") public WebElement vpnResilienceLnk;
	
//	Vpn Add product
	@FindBy(xpath = "//div[contains(@id,'siteCabinetType_VPN')]//following-sibling::a") public WebElement vpnCabinetTypeLst;
	@FindBy(xpath = "//ul[contains(@id,'siteCabinetType_VPN')]") public WebElement vpnCabinetTypeSubLst;
	@FindBy(xpath = "//input[contains(@id,'siteCabinetID_VPN')]") public WebElement vpnCabinetIdTxb;
	@FindBy(xpath = "//div[contains(@id,'siteCustomerSitePOPStatus_VPN')]//following-sibling::a") public WebElement vpnCustomerPopLst;
	@FindBy(xpath = "//ul[contains(@id,'siteCustomerSitePOPStatus_VPN')]") public WebElement vpnCustomerPopSubLst;
	@FindBy(xpath = "//div[contains(@id,'sitePresentationInterface_VPN')]//following-sibling::a") public WebElement vpnSitePresentationLst;
	@FindBy(xpath = "//ul[contains(@id,'sitePresentationInterface_VPN')]") public WebElement vpnSitePresentationSubLst;
	@FindBy(xpath = "//div[contains(@id,'siteConnectorType_VPN')]//following-sibling::a") public WebElement vpnSiteConnectorLst;
	@FindBy(xpath = "//ul[contains(@id,'siteConnectorType_VPN')]") public WebElement vpnSiteConnectorSubLst;
	@FindBy(xpath = "//input[contains(@id,'siteID_VPN')]") public WebElement vpnSiteIdTxb;
	
//	Vpn Dual Entry
	@FindBy(xpath = "//span[text()='Add-Ons']") public WebElement vpnAddONLink;
	@FindBy(xpath = "//div[@id='oj-select-choice-exploreCheckboxActionsDualEntryVPN']//a") public WebElement vpnDualEntryLink;
	@FindBy(xpath = "//input[@name='closeWindowForManualRequestDE']") public WebElement vpnExploreClsBtn;
	
//	Vpn Offnet Entry
	@FindBy(xpath = "//label[contains(@for,'connectivityCheckActions0')]") public WebElement vpnOffNetCheckBtn;
	@FindBy(xpath = "//label[contains(@for,'connectivityCheckActions3')]") public WebElement vpnManualCheckBtn;
	@FindBy(xpath = "//div[@id='oj-select-choice-raiseManualRequestVPN']//a") public WebElement vpnExploreRequestVPN;
	@FindBy(xpath = "//ul[contains(@id,'raiseManualRequestVPN')]") public WebElement vpnExploreRequestVPNSubLst;
	@FindBy(xpath = "//input[contains(@name,'closeWindowForMR')]") public WebElement vpnExploreOffCloseBtn;
	@FindBy(xpath = "//span[@class='oj-radiocheckbox-icon']//input[@name='contractTerm1Year-0']") public WebElement vpnOffnetRadioBtn;
	
//	Vpn DSL Entry
	@FindBy(xpath = "//label[contains(@for,'connectivityCheckActions1')]") public WebElement vpnOffNetDSLCheckBtn;
	
//	Address selection
	
	
	@FindBy(xpath = "//input[contains(@id,'siteAddressPrim')]") public WebElement siteAddressIPAcessTxb;
	@FindBy(xpath = "//input[contains(@aria-describedby,'siteAddressAEnd')]") public WebElement siteAAddressTxb;
	@FindBy(xpath = "//input[contains(@aria-describedby,'siteAddressBEnd')]") public WebElement siteBAddressTxb;
	@FindBy(xpath = "//input[contains(@id,'siteTelephoneNumberAEnd')]") public WebElement siteAPhoneNumberTxb;
	@FindBy(xpath = "descendant::h3[text()='Site Address Details'][1]") public WebElement siteAAddressLnk;
	@FindBy(xpath = "//input[contains(@id,'siteTelephoneNumberBEnd')]") public WebElement siteBPhoneNumberTxb;
	@FindBy(xpath = "descendant::h3[text()='Site Address Details'][2]") public WebElement siteBAddressLnk;
	@FindBy(xpath = "//img[@title='Search for an Address']") public WebElement siteSearchImg;
	@FindBy(xpath = "//div[@id='siteAddressAEnd_message']") public WebElement siteAddressMessage;
	@FindBy(xpath = "//span[text()='Site Address Details']") public WebElement siteAddressDetailsLnk;
	@FindBy(xpath = "//input[@id='premiseNumberAEnd']") public WebElement premiseNumberAEndTxb;
	@FindBy(xpath = "//input[@id='streetNameAEnd']") public WebElement streetNameAEndTxb;
	@FindBy(xpath = "//input[@id='cityAEnd']") public WebElement cityAEndTxb;
	@FindBy(xpath = "//input[@id='countryAEnd']") public WebElement countryAEndTxb;
	@FindBy(xpath = "//input[@id='postCodeAEnd']") public WebElement postCodeAEndTxb;
	@FindBy(xpath = "//label[text()='Site Address']") public WebElement siteAddressLabel;
	@FindBy(xpath = "//oj-checkboxset[@id='searchPrimary']") public WebElement sitePrimaryImg;
	@FindBy(xpath = "//div[@id='searchButtonAEnd']//img") public WebElement siteASearchImg;
	@FindBy(xpath = "//div[@id='searchButtonBEnd']//img") public WebElement siteBSearchImg;
	@FindBy(xpath = "//img//following-sibling::div[contains(text(),'Connected via Colt')]") public WebElement connectedViaColtElem;
	@FindBy(xpath = "//div//div[contains(text(),'Could not find Onnet and Automated Nearnet Connectivity')]") public WebElement connectionNotFound;
	@FindBy(xpath = "//div//span[contains(text(),'Datacenter Connected via Colt')]") public WebElement dataCenterConnectedElem;
	@FindBy(xpath = "//div//div[contains(text(),'Automated Nearnet')]") public WebElement automatedNearnetElem;
	@FindBy(xpath = "//td[text()='Next']") public WebElement nextBtn;
	
//	Onnet Dual Entry entries under faetures link
	@FindBy(xpath = "//span[text()='Features']") public WebElement featuresLnk;
	@FindBy(xpath = "//div[text()='B End']") public WebElement bEndLink;
	@FindBy(xpath = "//div[contains(@id,'exploreCheckboxActionsEndDE')]//following-sibling::a") public WebElement onnetManualRequestLst;
	@FindBy(xpath = "//ul[contains(@id,'exploreCheckboxActionsEndDE')]") public WebElement onnetManualRequestSubLst;
	@FindBy(xpath = "//input[@value='REVALIDATE']") public WebElement reValidateRBtn;
	@FindBy(xpath = "//input[@value='RENEGOTIATE']") public WebElement reNegotiateRBtn;	
	@FindBy(xpath = "//span[text()='Revalidate']") public WebElement exploreReValidateBtn;	
	@FindBy(xpath = "//span[text()='Renegotiate']") public WebElement exploreReNegotiateBtn;
	
//	A-End Features
	@FindBy(xpath = "//oj-switch[@id='outsideBusinessHoursInstallationAEnd']") public WebElement outsideBusinessHoursInstallationAEndCbx;	
	@FindBy(xpath = "//oj-switch[@id='longLiningAEnd']") public WebElement longLiningAEndCbx;	
	@FindBy(xpath = "//oj-switch[@id='internalCablingAEnd']") public WebElement internalCablingAEndCbx;
	@FindBy(xpath = "//div[contains(@id,'selectFloor_AEndPrim')]//following-sibling::a") public WebElement selectAEndFloorPrimaryLst;
	@FindBy(xpath = "//ul[contains(@id,'selectFloor_AEndPrim')]") public WebElement selectAEndFloorPrimarySubLst;
	@FindBy(xpath = "//oj-switch[@id='linkAggregationLAGAEnd']") public WebElement linkAggregationLAGAEndCbx;
	@FindBy(xpath = "//div[contains(@id,'selectFloor')]//following-sibling::a") public WebElement selectAEndFloorIpAccessLst;
	@FindBy(xpath = "//ul[contains(@id,'selectFloor')]") public WebElement selectAEndFloorIpAccessSubLst;
	
	//div[contains(@id,'selectFloor')]//following-sibling::a
	@FindBy(xpath = "//oj-switch[@id='outsideBusinessHoursInstallPrimary']") public WebElement outsideBusinessHourIpAccessCbx;	
	@FindBy(xpath = "//oj-switch[@id='longLiningPrimary']") public WebElement longLiningIpAccessCbx;	
	
//	B_End Features
	@FindBy(xpath = "//oj-switch[@id='outsideBusinessHoursInstallationBEnd']") public WebElement outsideBusinessHoursInstallationBEndCbx;	
	@FindBy(xpath = "//oj-switch[@id='longLiningBEnd']") public WebElement longLiningBEndCbx;	
	@FindBy(xpath = "//oj-switch[@id='internalCablingBEnd']") public WebElement internalCablingBEndCbx;	
	@FindBy(xpath = "//div[contains(@id,'selectFloor_BEndPrim')]//following-sibling::a") public WebElement selectBEndFloorPrimaryLst;
	@FindBy(xpath = "//ul[contains(@id,'selectFloor_BEndPrim')]") public WebElement selectBEndFloorPrimarySubLst;
	@FindBy(xpath = "//oj-switch[@id='linkAggregationLAGBEnd']") public WebElement linkAggregationLAGBEndCbx;
	
	
//	Offnet and Nearnet Process
	@FindBy(xpath = "//td[text()='Waiting for 3rd Party']/following-sibling::td//*[contains(@name,'connRadioVarNameAEnd') and contains(@value,'1')]") public WebElement etherNetAManualOffnetRdB;
	@FindBy(xpath = "//td[text()='Waiting for 3rd Party']/following-sibling::td//*[contains(@name,'connRadioVarNameBEnd') and contains(@value,'1')]") public WebElement etherNetBManualOffnetRdB;
	@FindBy(xpath = "//label[contains(@for,'offNetDSLCCRequestAEnd0')]") public WebElement offnetCheckAEndBtn;
	@FindBy(xpath = "//label[contains(@for,'offNetDSLCCRequestAEnd2')]") public WebElement manualEngagementAEndBtn;
	@FindBy(xpath = "//label[contains(@for,'offNetDSLCCRequestAEnd1')]") public WebElement dslCheckAEndBtn;
	@FindBy(xpath = "//label[contains(@for,'offNetDSLCCRequestBEnd1')]") public WebElement dslCheckBEndBtn;
	@FindBy(xpath = "//div[@id='nearNetA']//input[@name='connRadioVarNameAEnd']") public WebElement automatedNearnetCheckAEndBtn;
	@FindBy(xpath = "//div[@id='nearNetB']//input[@name='connRadioVarNameBEnd']") public WebElement automatedNearnetCheckBEndBtn;
	@FindBy(xpath = "//div[contains(@id,'exploreCheckboxActionsEndA')]//following-sibling::a") public WebElement exploreActionsAEndLst;
	@FindBy(xpath = "//ul[contains(@id,'exploreCheckboxActionsEndA')]") public WebElement exploreActionsAEndSubLst;
	@FindBy(xpath = "//label[contains(@for,'offNetDSLCCRequestBEnd0')]") public WebElement offnetCheckBEndBtn;
	@FindBy(xpath = "//label[contains(@for,'offNetDSLCCRequestBEnd2')]") public WebElement manualEngagementBEndBtn;
	@FindBy(xpath = "//div[contains(@id,'exploreCheckboxActionsEndB')]//following-sibling::a") public WebElement exploreActionsBEndLst;
	@FindBy(xpath = "//ul[contains(@id,'exploreCheckboxActionsEndB')]") public WebElement exploreActionsBEndSubLst;
	@FindBy(xpath = "//span[text()='Priority']//preceding-sibling::input") public WebElement priorityTxb;
	@FindBy(xpath = "//span[@id='dualEntryStandard']//child::span[contains(@class,'CheckBox')]") public WebElement dualEntryCbx;
	@FindBy(xpath = "//span[text()='Get Quote']") public WebElement getQuoteBtn;
	@FindBy(xpath = "//div[contains(@id,'servicesDialog_')]//..//div[contains(@class,'dgrid-expando-icon ui-icon ui-icon-triangle')]") public WebElement expandArrowExploreIcn;
//	@FindBy(xpath = "//div[contains(@id,'servicesDialog_')]//..//div[contains(@class,'grid-expand')]") public WebElement expandArrowExploreIcn;	
	@FindBy(xpath = "//div[contains(@id,'servicesDialog_')]//..//div[contains(@class,'grid-expand')]//parent::td//following-sibling::td") public WebElement exploreRequestIDTdElem;
	@FindBy(xpath = "//span[contains(text(),'Request ID:')]") public WebElement requestIDElem;
//	@FindBy(xpath = "//input[@value='X']//parent::span//following-sibling::label") public WebElement IpAccessexploreCloseBtn;
	@FindBy(xpath = "//label[contains(@for,'addToTableCheckBox_explore0')]") public WebElement IpAccessexploreCloseBtn;
	@FindBy(xpath = "//td[text()='CLOSE']") public WebElement exploreCloseBtn;
	@FindBy(xpath = "//div[@id='application']//.//span[text()='Submit']") public WebElement exploreSubmitBtn;
	@FindBy(xpath = "//input[@name='connOption']") public WebElement ipAccessOnnetRBtn;
	@FindBy(xpath = "descendant::div[@id='onNetA']//input[@name='connRadioVarNameAEnd'][1]") public WebElement onnetAEndRBtn;
	@FindBy(xpath = "descendant::div[@id='onNetB']//input[@name='connRadioVarNameBEnd'][1]") public WebElement onnetBEndRBtn;
	@FindBy(xpath = "descendant::div[@id='offNetA']//input[@name='connRadioVarNameAEnd'][1]") public WebElement automatedOffnetDefAEndRadioBtn;
	@FindBy(xpath = "descendant::div[@id='offNetB']//input[@name='connRadioVarNameBEnd'][1]") public WebElement automatedOffnetDefBEndRadioBtn;
	@FindBy(xpath = "descendant::div[@id='dslNetA']//input[@name='connRadioVarNameAEnd'][1]") public WebElement automatedDSLDefAEndRadioBtn;
	@FindBy(xpath = "descendant::div[@id='dslNetB']//input[@name='connRadioVarNameBEnd'][1]") public WebElement automatedDSLDefBEndRadioBtn;
	@FindBy(xpath = "//td[text()='Waiting for 3rd Party']/following-sibling::td//*[contains(@name,'connRadioVarNameAEnd')]") public WebElement etherNetSpokeManualOffnet;
	@FindBy(xpath = "(//td[text()='ACTUAL COST'])[2]/following-sibling::td//*[contains(@name,'connRadioVarNameAEnd')]") public WebElement etherNetSpokeRevalidateOffnet;
	@FindBy(xpath = "//span[text()='Connection Type']//preceding-sibling::input") public WebElement connectionType;
	@FindBy(xpath = "//input[@id='radioAEnd_0DSLCC']") public WebElement etherNetSpokeDSLOffnet;
	@FindBy(xpath = "//div[contains(@id,'Replacement_capex_interface')]//following-sibling::a") public WebElement offnetWaveincrementalCapex;
	@FindBy(xpath = "//ul[contains(@id,'incrementalReplacement_capex_interface')]") public WebElement offnetWaveCapexSubLst;
	@FindBy(xpath = "//div[contains(@id,'incrementalReplacement_opex_interface')]//following-sibling::a") public WebElement offnetWaveincrementalOpex;
	@FindBy(xpath = "//ul[contains(@id,'incrementalReplacement_opex_interface')]") public WebElement offnetWaveOpexSubLst;
	@FindBy(xpath = "//div[contains(@id,'frequency_interface')]//following-sibling::a") public WebElement offnetWaveFrequency;
	@FindBy(xpath = "//ul[contains(@id,'frequency_interface')]") public WebElement offnetWaveFrequencySubLst;

	
//	Offnet IP Access
	@FindBy(xpath = "//td[@id='offnetcheckAEnd']") public WebElement ipOffnetCheckAEndBtn;
	@FindBy(xpath = "//td[@id='manualEngagementAEnd']") public WebElement ipManualEngagementAEndBtn;
	@FindBy(xpath = "//div[contains(@id,'exploreManualEngagementCheckboxActionsAEnd')]//following-sibling::a") public WebElement ipExploreActionsAEndLst;
	@FindBy(xpath = "//ul[contains(@id,'exploreManualEngagementCheckboxActionsAEnd')]") public WebElement ipExploreActionsAEndSubLst;
	@FindBy(xpath = "//input[contains(@id,'closeButtonForAEnd')]") public WebElement ipSupplierOnnetClose ;
	@FindBy(xpath = "//td[@id='dslcheckAEnd']") public WebElement ipDSLCheck ;
	

//	Partial Save Button
	@FindBy(xpath = "//input[@name='partialSave_SiteAddress']") public WebElement partialSaveAddressCbx;
	@FindBy(xpath = "//input[@name='markForPartialSaveSysConfig']") public WebElement ipPartialSaveAddressCbx;
	@FindBy(xpath = "//input[@name='partialSave_SiteDetails']") public WebElement partialSaveSiteCbx;
	@FindBy(xpath = "//input[@name='partialSave_Features']") public WebElement partialSaveFeaturesCbx;
	@FindBy(xpath = "//oj-switch[@id='partialSave_AdditionalTab']") public WebElement partialSaveAdditionalCbx;
	@FindBy(xpath = "//span[text()='Site Details']") public WebElement siteDetailsLnk;
	
//	Ethernet Line Override objects
	@FindBy(xpath = "//input[@name='overrideToULLAEnd']") public WebElement overrideToULLAEndCbx;
	@FindBy(xpath = "//textarea[@id='overrideReasonAEnd']") public WebElement overrideReasonAEndTxb;
	@FindBy(xpath = "//input[@name='overrideToULLBEnd']") public WebElement overrideToULLBEndCbx;
	@FindBy(xpath = "//textarea[@id='overrideReasonBEnd']") public WebElement overrideReasonBEndTxb;
	@FindBy(xpath = "//input[@name='overrideToULLAEnd']") public WebElement overrideToOnnetAEndCbx;
	@FindBy(xpath = "//input[@name='overrideToULLBEnd']") public WebElement overrideToOnnetBEndCbx;
	@FindBy(xpath = "//a[text()='Update']") public WebElement updateBtn;
	@FindBy(xpath = "//input[contains(@id,'InputtedIsCorrectAEnd')]") public WebElement AEndCorrectCbx;
	@FindBy(xpath = "//input[contains(@id,'InputtedIsCorrectBEnd')]") public WebElement BEndCorrectCbx;	
	
//	CPE Configuration
	@FindBy(xpath = "//span[text()='CPE CONFIGURATION']") public WebElement cpeConfigurationLnk;
	@FindBy(xpath = "//div[contains(@id,'connectionType')]//following-sibling::a") public WebElement connectionTypeLst;
	@FindBy(xpath = "//ul[contains(@id,'connectionType')]") public WebElement connectionTypeSubLst;
//	@FindBy(xpath = "//select[@id='serviceBandwidth']") public WebElement serviceBandwidthLst;
	@FindBy(xpath = "//div[contains(@id,'serviceBandwidth')]//following-sibling::a") public WebElement serviceBandwidthLst;
	@FindBy(xpath = "//input[contains(@aria-controls,'serviceBandwidth')]") public WebElement serviceBandwidthTxb;
	@FindBy(xpath = "//input[contains(@aria-controls,'serviceBandwidth')]//following-sibling::span//child::span") public WebElement serviceBandwidthQry;
	@FindBy(xpath = "//ul[contains(@id,'serviceBandwidth')]") public WebElement serviceBandwidthSubLst;
	@FindBy(xpath = "//div[contains(@id,'managedCPE')]//following-sibling::a") public WebElement managedCPELst;
	@FindBy(xpath = "//ul[contains(@id,'managedCPE')]") public WebElement managedCPESubLst;
	@FindBy(xpath = "//div[contains(@id,'cPESolutionType')]//following-sibling::a") public WebElement cpeSolutionTypeLst;
	@FindBy(xpath = "//ul[contains(@id,'cPESolutionType')]") public WebElement cpeSolutionTypeSubLst;
	@FindBy(xpath = "//label[text()='CPE Model']") public WebElement cpeModelLabel;
	@FindBy(xpath = "//div[contains(@id,'interface')]//following-sibling::a") public WebElement interfaceLst;
	@FindBy(xpath = "//ul[contains(@id,'interface')]") public WebElement interfaceSubLst;
	@FindBy(xpath = "//div[contains(@id,'resiliencyServiceLevel')]//following-sibling::a") public WebElement resiliancyTypeLst;
	@FindBy(xpath = "//ul[contains(@id,'resiliencyServiceLevel')]") public WebElement resiliancyTypeSubLst;
	@FindBy(xpath = "//div[contains(@id,'contractTermInYears')]//following-sibling::a") public WebElement contractTermLst;
	@FindBy(xpath = "//ul[contains(@id,'contractTermInYears')]") public WebElement contractTermSubLst;
	
	
//  Ip Access
	@FindBy(xpath = "//span[text()='L3 Resilience']") public WebElement ipL3ResilienceLnk;
	@FindBy(xpath = "//a[@title='Primary Features']") public WebElement primaryFeaturesLnk;
	@FindBy(xpath = "//span[text()='Diversity']") public WebElement ipDiversityLnk;
	@FindBy(xpath = "//div[contains(@id,'contractTerm')]//following-sibling::a") public WebElement ipContractTermLst;
	@FindBy(xpath = "//ul[contains(@id,'contractTerm')]") public WebElement ipContractTermSubLst;
	@FindBy(xpath = "//div[contains(@id,'billingType')]//following-sibling::a") public WebElement ipBillingTypeLst;
	@FindBy(xpath = "//ul[contains(@id,'billingType')]") public WebElement ipBillingTypeSubLst;
	@FindBy(xpath = "//div[contains(@id,'layer2Resilience')]//following-sibling::a") public WebElement ipL2ReilienceLst;
	@FindBy(xpath = "//ul[contains(@id,'layer2Resilience')]") public WebElement ipL2ReilienceSubLst;
	@FindBy(xpath = "//span[text()='Next']//parent::oj-option") public WebElement cpeIPAcessNextBtn;
	@FindBy(xpath = "//div[@class='crumb active']//a/span[contains(text(),'Primary Connection')]") public WebElement ipPrimaryConnectionLnk;
	@FindBy(xpath = "//td[text()='APPROVED']") public WebElement ipApprovedTxt;
	@FindBy(xpath = "//span[text()='Site Addons']//parent::a//parent::div") public WebElement ipSiteAddonsLnk;
	@FindBy(xpath = "//span[text()='Service Addons']//parent::a//parent::div") public WebElement ipServiceAddonsLnk;
	@FindBy(xpath = "//div[contains(@id,'committedBandwidth')]//following-sibling::a") public WebElement ipcomittedBandwidthLnk;
	@FindBy(xpath = "//ul[contains(@id,'committedBandwidth')]") public WebElement ipcomittedBandwidthSubLnk;
	@FindBy(xpath = "//div[contains(@id,'maximumBandwidth')]//following-sibling::a") public WebElement ipmaximumBandwidthLnk;
	@FindBy(xpath = "//ul[contains(@id,'maximumBandwidth')]") public WebElement ipmaximumBandwidthSubLnk;
	
//  Ip Access Router Type Configuration
	@FindBy(xpath = "//div[contains(@id,'routerTypePrimary')]//following-sibling::a") public WebElement ipIPRouterTypeLst;
	@FindBy(xpath = "//ul[contains(@id,'routerTypePrimary')]") public WebElement ipIPRouterTypeSubLst;
	@FindBy(xpath = "//div[contains(@id,'routerLANInterfaceTypePrimary')]//following-sibling::a") public WebElement ipIPRouterLanInterfaceTypeLst;
	@FindBy(xpath = "//ul[contains(@id,'routerLANInterfaceTypePrimary')]") public WebElement ipIPRouterLanInterfaceTypeSubLst;
	@FindBy(xpath = "//select[@id='selectInterfaceTypeAEnd']") public WebElement ipSelectInterFaceLst;
	@FindBy(xpath = "//oj-switch[@id='nATPrimary']") public WebElement ipNATCbx;
	@FindBy(xpath = "//oj-switch[@id='dHCPPrimary']") public WebElement dhcpCbx;	
	@FindBy(xpath = "//oj-switch[@id='sNMPPrimary']") public WebElement snmpCbx;
	@FindBy(xpath = "//oj-switch[@id='cloudPrioritizationPrimary']") public WebElement cloudPrioritizationCbx;
	@FindBy(xpath = "//input[contains(@id,'coltCPEIDAEnd')]") public WebElement ipColtIdTbx;
	@FindBy(xpath = "//input[contains(@id,'costOfCPEAEnd')]") public WebElement ipColtCostTbx;
	@FindBy(xpath = "//input[contains(@id,'routerModelAEnd')]") public WebElement ipColtRouterModelLst;
	@FindBy(xpath = "//div[contains(@aria-describedby,'fastTrack')]") public WebElement fastTrackCbx;

//  IP Access BGP4 Type
	@FindBy(xpath = "//div[contains(@aria-describedby,'bGP4Feed')]") public WebElement ipBGP4FeedCbx;
	@FindBy(xpath = "//div[contains(@id,'bGP4FeedType')]//following-sibling::a") public WebElement ipBGP4FeedTypeLst;
	@FindBy(xpath = "//ul[contains(@id,'bGP4FeedType')]") public WebElement ipBGP4FeedTypeSubLst;
	@FindBy(xpath = "//div[contains(@id,'typeOfAS')]//following-sibling::a") public WebElement cpeBGPTypeAsEndLst;
	@FindBy(xpath = "//ul[contains(@id,'typeOfAS')]") public WebElement cpeBGPTypeAsEndSubLst;

//  IP Access SMTP Type
	@FindBy(xpath = "//oj-switch[@id='sMTPPrimary']") public WebElement ipSMTPFeedCbx;
	
//  PRESENTATION INTERFACE
	@FindBy(xpath = "//select[@id='servicePresentationInterfaceType']") public WebElement ipServicePresenattionLst;
	@FindBy(xpath = "//div[contains(@id,'interfaceAEnd')]//following-sibling::a") public WebElement ipinterfaceAEndLst;
	@FindBy(xpath = "//ul[contains(@id,'interfaceAEnd')]") public WebElement ipinterfaceAEndSubLst;
	@FindBy(xpath = "//div[contains(@id,'connectorAEnd')]//following-sibling::a") public WebElement ipConnectorLst;
	@FindBy(xpath = "//ul[contains(@id,'connectorAEnd')]") public WebElement ipConnectorSubLst;
	@FindBy(xpath = "//span[text()='PRESENTATION INTERFACE']") public WebElement ipPresentationInterFaceLst;

//  IP ADDRESSING
	@FindBy(xpath = "//div[contains(@id,'iPAddressingFormatAEnd')]//following-sibling::a") public WebElement ipAddressingFormatLst;
	@FindBy(xpath = "//ul[contains(@id,'iPAddressingFormatAEnd')]") public WebElement ipAddressingFormatSubLst;
	@FindBy(xpath = "//div[contains(@id,'iPv4AddressingTypeAEnd')]//following-sibling::a") public WebElement ipAddressingTypeAEndLst;
	@FindBy(xpath = "//ul[contains(@id,'iPv4AddressingTypeAEnd')]") public WebElement ipAddressingTypeAEndSubLst;
	@FindBy(xpath = "//div[contains(@id,'numberOfIPv4Addresses')]//following-sibling::a") public WebElement ipAddressingIPv4Lst;
	@FindBy(xpath = "//ul[contains(@id,'numberOfIPv4Addresses')]") public WebElement ipAddressingIPv4SubLst;
	@FindBy(xpath = "//input[@name='carrierHotelCrossConnectAEnd']") public WebElement ipCarrierHotelLst;
	
//  L3 Resiliency Id
	@FindBy(xpath = "//a[@title='L3 Resiliency']") public WebElement L3ResilienceLnk;
	@FindBy(xpath = "//div[contains(@id,'layer3Resiliency')]//following-sibling::a") public WebElement ipL3ResilianceLnk;
	@FindBy(xpath = "descendant::span[text()='L3 Resilience']//parent::a//parent::div[1]") public WebElement ipL3ResilianceParentLink;	
	@FindBy(xpath = "//ul[contains(@id,'layer3Resiliency')]") public WebElement ipL3ResilianceSubLnk;
	@FindBy(xpath = "//div[contains(@id,'backupBandwidth')]//following-sibling::a") public WebElement ipBackUpBandWidthLst;
	@FindBy(xpath = "//ul[contains(@id,'backupBandwidth')]") public WebElement ipBackUpBandWidthSubLst;
	@FindBy(xpath = "//span[text()='IP Features']") public WebElement ipFeaturesLnk;
	
//  Diversity
	@FindBy(xpath = "//div[contains(@aria-describedby,'diversity')]") public WebElement ipDiversitySelector;
	
//	Service Adons
	@FindBy(xpath = "//input[@id='customerRequiredDate']") public WebElement ipCustomerRequired;
	
// 	Bespoke Feature
	@FindBy(xpath = "//div[contains(@aria-describedby,'bespokeFeatureRequired')]") public WebElement ipBeSpokeFeature;
	
// 	Ip Access Additional Product Data Primary
	@FindBy(xpath = "//div[contains(@id,'siteCabinetType')]//following-sibling::a") public WebElement ipCabinetType;
	@FindBy(xpath = "//ul[contains(@id,'siteCabinetType')]") public WebElement ipCabinetSubType;
	@FindBy(xpath = "//input[contains(@id,'siteCabinetID')]") public WebElement ipCabinetID;
	@FindBy(xpath = "//div[contains(@id,'siteAccessTechnology')]//following-sibling::a") public WebElement ipSiteAccessTechnologyLst;
	@FindBy(xpath = "//ul[contains(@id,'siteAccessTechnology')]") public WebElement ipSiteAccessTechnologySubLst;
	
// 	Ip Access Additional Product Data Secondary
	@FindBy(xpath = "//div[contains(@id,'routerTechnology')]//following-sibling::a") public WebElement ipRouterTechnologyLst;
	@FindBy(xpath = "//ul[contains(@id,'routerTechnology')]") public WebElement ipRouterTechnologySubLst;
	@FindBy(xpath = "//div[contains(@id,'existingCapacityLeadTimeAEnd')]//following-sibling::a") public WebElement ipexistingCapacityLeadTimeLst;
	@FindBy(xpath = "//ul[contains(@id,'existingCapacityLeadTimeAEnd')]") public WebElement ipexistingCapacityLeadTimeSubLst;
	@FindBy(xpath = "//input[contains(@id,'firstName')]") public WebElement ipFirstName;
	@FindBy(xpath = "//input[contains(@id,'lastName')]") public WebElement ipLastName;
	@FindBy(xpath = "//input[contains(@id,'contactNumber')]") public WebElement ipContactNumber;
	@FindBy(xpath = "//input[contains(@id,'mobileNumber')]") public WebElement ipMobileNumber;
	@FindBy(xpath = "//input[contains(@id,'email')]") public WebElement ipEmail;
	@FindBy(xpath = "//input[contains(@id,'faxNumber')]") public WebElement ipFaxNumber;
	@FindBy(xpath = "//div[contains(@aria-describedby,'voiceBundledService')]") public WebElement ipVoiceBundleService;
	@FindBy(xpath = "//span[text()='Secondary Site']") public WebElement ipSecondarySite;
	
		
//	Additional Product Data
	@FindBy(xpath = "//span[text()='Additional Product Data']") public WebElement addtionalProductDataLnk;
	@FindBy(xpath = "//span[text()='Service Information']") public WebElement serviceInformationLnk;
	@FindBy(xpath = "//div[contains(@id,'existingCapacityLeadTime')]//following-sibling::a") public WebElement existingCapacityLeadTimeLst;
	@FindBy(xpath = "//ul[contains(@id,'existingCapacityLeadTime')]") public WebElement existingCapacityLeadTimeSubLst;
	@FindBy(xpath = "//label[contains(text(),'Existing Capacity Lead Time')]']") public WebElement existingCapacityLeadTimeLabel;
	@FindBy(xpath = "//span[text()='Service Features']") public WebElement serviceFeaturesLnk;
	
//	AEnd Details Entry
	
	@FindBy(xpath = "//div[contains(@id,'siteType')]//following-sibling::a") public WebElement siteTypeLst;
	@FindBy(xpath = "//ul[contains(@id,'siteType')]") public WebElement siteTypeSubLst;
	@FindBy(xpath = "//input[contains(@id,'siteCabinetIDAEndPrimary')]") public WebElement cabinetIDATxb;
	@FindBy(xpath = "//div[contains(@id,'siteCabinetTypeAEndPrimary')]//following-sibling::a") public WebElement cabinetTypeALst;
	@FindBy(xpath = "//ul[contains(@id,'siteCabinetTypeAEndPrimary')]") public WebElement cabinetTypeASubLst;
	@FindBy(xpath = "//div[contains(@id,'siteAccessTechnologyPrimaru')]//following-sibling::a") public WebElement accessTechnologyALst;
	@FindBy(xpath = "//ul[contains(@id,'siteAccessTechnologyPrimaru')]") public WebElement accessTechnologyASubLst;
	@FindBy(xpath = "//div[contains(@id,'siteCustomerSitePOPStatusPrimary')]//following-sibling::a") public WebElement customerPopStatusALst;
	@FindBy(xpath = "//ul[contains(@id,'siteCustomerSitePOPStatusPrimary')]") public WebElement customerPopStatusASubLst;
	@FindBy(xpath = "//input[contains(@id,'siteIdAEnd')]") public WebElement siteIDATxb;
	@FindBy(xpath = "//div[contains(@id,'sitePresentationInterfacePrimary')]//following-sibling::a") public WebElement presentationInterfaceALst;
	@FindBy(xpath = "//ul[contains(@id,'sitePresentationInterfacePrimary')]") public WebElement presentationInterfaceASubLst;
	@FindBy(xpath = "//div[contains(@id,'siteConnectorType')]//following-sibling::a") public WebElement connectorTypeALst;
	@FindBy(xpath = "//ul[contains(@id,'siteConnectorType')]") public WebElement connectorTypeASubLst;
	@FindBy(xpath = "//div[contains(@id,'sitePortRole')]//following-sibling::a") public WebElement portRoleALst;
	@FindBy(xpath = "//ul[contains(@id,'sitePortRole')]") public WebElement portRoleASubLst;
	
//	BEnd Details Entry
	@FindBy(xpath = "//input[contains(@id,'siteCabinetIDBEndPrimary')]") public WebElement cabinetIDBTxb;
	@FindBy(xpath = "//div[contains(@id,'siteCabinetTypeBEndPrimary')]//following-sibling::a") public WebElement cabinetTypeBLst;
	@FindBy(xpath = "//ul[contains(@id,'siteCabinetTypeBEndPrimary')]") public WebElement cabinetTypeBSubLst;
	@FindBy(xpath = "//div[contains(@id,'siteAccessTechnologyBendPrimary')]//following-sibling::a") public WebElement accessTechnologyBLst;
	@FindBy(xpath = "//ul[contains(@id,'siteAccessTechnologyBendPrimary')]") public WebElement accessTechnologyBSubLst;
	@FindBy(xpath = "//div[contains(@id,'siteCustomerSitePOPStatusBEndPrimary')]//following-sibling::a") public WebElement customerPopStatusBLst;
	@FindBy(xpath = "//ul[contains(@id,'siteCustomerSitePOPStatusBEndPrimary')]") public WebElement customerPopStatusBSubLst;
	@FindBy(xpath = "//input[contains(@id,'siteIdBPrimary')]") public WebElement siteIDBTxb;
	@FindBy(xpath = "//div[contains(@id,'sitePresentationInterfaceBEndPrimary')]//following-sibling::a") public WebElement presentationInterfaceBLst;
	@FindBy(xpath = "//ul[contains(@id,'sitePresentationInterfaceBEndPrimary')]") public WebElement presentationInterfaceBSubLst;
	@FindBy(xpath = "//div[contains(@id,'siteConnectorTypeBEndPrimary')]//following-sibling::a") public WebElement connectorTypeBLst;
	@FindBy(xpath = "//ul[contains(@id,'siteConnectorTypeBEndPrimary')]") public WebElement connectorTypeBSubLst;
	@FindBy(xpath = "//div[contains(@id,'sitePortRoleBEndPrimary')]//following-sibling::a") public WebElement portRoleBLst;
	@FindBy(xpath = "//ul[contains(@id,'sitePortRoleBEndPrimary')]") public WebElement portRoleBSubLst;
	
//	Saving the Quote
	@FindBy(xpath = "//span[text()='Save to Quote']") public WebElement saveToQuoteBtn;
	@FindBy(xpath = "//oj-button[@name='_add_to_transaction']") public WebElement saveToQuoteBtn1;	
	@FindBy(xpath = "//div[contains(@class,'error-content')]") public WebElement errorContentCPQ;
	@FindBy(xpath = "//oj-button[@title='Reconfigure']") public WebElement reConfigureBtn;
	@FindBy(xpath = "//img[@class='page-progress-complete']") public WebElement imgLoadComplete;
	@FindBy(xpath = "//label[contains(@id,'voiceBundledService-label')]") public WebElement voiceBundledServiceElem;
	
//	Portfolio Pricing
	@FindBy(xpath = "//span[text()='Engage Portfolio Pricing']") public WebElement engagePortfolioPricingBtn;
	@FindBy(xpath = "//div[contains(@id,'portfolioTeamAssignment')]//child::a") public WebElement portfolioAssignmentLst;
	@FindBy(xpath = "//ul[contains(@id,'portfolioTeamAssignment')]") public WebElement portfolioAssignmentSubLst;
	@FindBy(xpath = "//span[text()='Assign Quote']") public WebElement assignQuoteBtn;
	@FindBy(xpath = "//span[text()='Refresh All Prices']") public WebElement refreshAllPricesBtn;
	@FindBy(xpath = "//span[text()='Send To Sales']") public WebElement sendToSalesBtn;	
	
//	Error Message CPQ
	@FindBy(xpath = "//div[contains(@class,'message-summary message-error')]") public WebElement cpqSummaryErrorMsgElem;
	@FindBy(xpath = "//div[contains(@class,'message-detail')]") public WebElement cpqErrorMsgElem;
	
//	Discounting
	@FindBy(xpath = "//span[text()='Quote']") public WebElement quoteLnk;
	@FindBy(xpath = "//input[contains(@id,'discountNRC')]") public WebElement basePriceNRCDiscountTxb;	
	@FindBy(xpath = "//input[contains(@id,'discountMRC')]") public WebElement basePriceMRCDiscountTxb;	
	@FindBy(xpath = "descendant::span[text()='Calculate Discount'][1]") public WebElement calculateDiscountBtn;	
	
//	Copy Quotes
	@FindBy(xpath = "//span[text()='Copy Line Items']//parent::span") public WebElement copyLineItemsBtn;
	@FindBy(xpath = "//input[contains(@aria-label,'Number Of Copies')]") public WebElement numberOfCopiesTxb;	
	@FindBy(xpath = "//oj-dialog[@id='copy-lines-form']//span[text()='OK']//parent::span") public WebElement copyPromptOKBtn;
	@FindBy(xpath = "//div[contains(text(),'You must select exactly one line item')]") public WebElement selectOneLineItemPrompt;
	
	
//	Option Quote
	@FindBy(xpath = "//div[@aria-labelledby='optionsQuote_t-label']") public WebElement optionQuoteRBtn;
	@FindBy(xpath = "//div[@aria-labelledby='projectQuote_t-label']") public WebElement projectQuoteRBtn;

	@FindBy(xpath = "//input[@id='numberOfOptions_t']") public WebElement noOfOptionsTxb;
	@FindBy(xpath = "//div[contains(@id,'selectPrimaryOptions')]//child::a") public WebElement selectPrimaryOptionsLst;
	@FindBy(xpath = "//ul[contains(@id,'selectPrimaryOptions')]") public WebElement selectPrimaryOptionsSubLst;
	@FindBy(xpath = "//input[@value='Discount']//parent::span") public WebElement discountCbx;
	@FindBy(xpath = "//span[text()='Calculate Discount']") public WebElement calculateLineLevelDiscountBtn;
	
//	Commercial Approval
	@FindBy(xpath = "//span[text()='Commercial Approval']//parent::a") public WebElement commercialValidationLnk;
	@FindBy(xpath = "//span[text()='Approval']//parent::a") public WebElement approvalLnk;
	@FindBy(xpath = "//div[contains(text(),'VP Sales -')]") public WebElement VPSalesTxtElem;
	@FindBy(xpath = "//div[@id='approval-comment']//span[text()='Submit']") public WebElement VPSalesSubmitBtn;
	@FindBy(xpath = "//button[@title='Approve']") public WebElement approveDiscountBtn;
	@FindBy(xpath = "//oj-button[@name='submit_to_approval']") public WebElement submitToApprovalBtn;
	@FindBy(xpath = "//a[@title='General Information']") public WebElement generalInformationLnk;
	
//	Legal and Technical Contact Info
	@FindBy(xpath = "//label[text()='Legal Contact Details']") public WebElement legalContactLnk;
	@FindBy(xpath = "//label[contains(@id,'technicalContactsDetails')]") public WebElement technicalContactLnk;
	@FindBy(xpath = "//input[@name='searchEmail']") public WebElement emailContactTxb;
	@FindBy(xpath = "//button[@id='searchButton']") public WebElement searchContactBtn;
	@FindBy(xpath = "//table[@id='contactList']//..//i[1]") public WebElement pickDefaultContactCbx;
	@FindBy(xpath = "//div[contains(@id,'legalGetContacts')]//child::input[@value='Get Contact Details']") public WebElement legalGetContactBtn;
	@FindBy(xpath = "//div[contains(@id,'technicalGetContacts')]//child::input[@value='Get Contact Details']") public WebElement technicalGetContactBtn;
	
//	Technical Approval
	@FindBy(xpath = "//a[@title='Technical Approval']") public WebElement technicalApprovalLnk;
	@FindBy(xpath = "//a[@title='SE Engagement']") public WebElement seEngagementLnk;
	@FindBy(xpath = "//div[contains(@id,'seReviewSelection')]//child::a") public WebElement seReviewLst;
	@FindBy(xpath = "//ul[contains(@id,'seReviewSelection')]") public WebElement seReviewSubLst;
	@FindBy(xpath = "//div[contains(@id,'reasonForSEReview')]//child::a") public WebElement seReasonLst;
	@FindBy(xpath = "//ul[contains(@id,'reasonForSEReview')]") public WebElement seReasonSubLst;
	@FindBy(xpath = "//oj-button[@name='confirm_selection']") public WebElement confirmSelectionBtn;
	@FindBy(xpath = "//oj-button[@name='technical_feasibility_complete']") public WebElement techFeasibilityComplete;
	@FindBy(xpath = "//input[@value='readyForTechnicalApproval']//parent::span") public WebElement readyForTechApprovalCbx;
	@FindBy(xpath = "//input[@value='customerCommittedInitiateFastSignClosureOfTheOpportunity']//parent::span") public WebElement fastSignInitiateCbx;
	@FindBy(xpath = "//button[@name='submit_for_technical_approval']") public WebElement submitTechApprovalBtn;
	@FindBy(xpath = "//div[@id='notificationsAndMessages_t']") public WebElement notificationBarCPQ;
	@FindBy(xpath = "//span[text()='Submit to CST Approval']") public WebElement submitCSTApprovalBtn;
	@FindBy(xpath = "//button[@name='add_internal_note']") public WebElement addInternalNoteBtn;
	@FindBy(xpath = "//textarea[contains(@id,'qtonotes')]") public WebElement quoteNotesTxb;
	@FindBy(xpath = "//span[text()='Add']") public WebElement addNotesBtn;
	@FindBy(xpath = "//span[text()='Approve']") public WebElement approveCSTBtn;
	@FindBy(xpath = "//div[contains(@id,'choice-selectOptionsTechApproval')]//child::a") public WebElement selectOptionsTechValLst;
	@FindBy(xpath = "//ul[contains(@id,'selectOptionsTechApproval')]") public WebElement selectOptionsTechValSubLst;
	@FindBy(xpath = "//span[text()='Proceed To Quote']") public WebElement proceedToQuoteBtn;

//	Project Quote 
	@FindBy(xpath = "//button[@name='proceed_to_quote']") public WebElement projectQuoteBtn;

	
//	FastSign Process
	@FindBy(xpath = "//span[text()='Close Fast Sign Process']") public WebElement closeFastSignBtn;
	@FindBy(xpath = "//div[contains(@id,'reason')]//child::a") public WebElement fastSignReasonLst;
	@FindBy(xpath = "//ul[contains(@id,'reason')]") public WebElement fastSignReasonSubLst;
	@FindBy(xpath = "//input[@id='customerCommitmentProof_t']") public WebElement customerProofUploadTxb;
	
//	top page
	@FindBy(xpath = "//div[@id='requestSupportAttribute']") public WebElement topPageScrollElem;
	
//	switch to proxy user
	@FindBy(xpath = "//img[@title='Admin']") public WebElement adminBtn;
	@FindBy(xpath = "//a[text()='Internal Users']") public WebElement internalUsersLnk;
	@FindBy(xpath = "//a[contains(text(),'Manager')]") public WebElement orderToQuoteManagerLnk;
	@FindBy(xpath = "//img[@title='Proxy Logout']") public WebElement proxyLogoutBtn;
	@FindBy(xpath = "//a[text()='Transactions']") public WebElement transactionsLnk;
	@FindBy(xpath = "descendant::a[text()='Copy'][1]") public WebElement copyTransactionsLnk;

	@FindBy(xpath = "//img[@title='Log out']//following-sibling::span") public WebElement cpqLogoutBtn;
	@FindBy(xpath = "//img[@title='Log out']") public WebElement cpqRFSLogoutBtn;
	@FindBy(xpath = "//img[@alt='Proxy Logout']") public WebElement proxyLogoutRFSBtn;
	@FindBy(xpath = "//img[@title='Log out']") public WebElement cpqLogoutRFSBtn;
	@FindBy(xpath = "//a[@id='new_transaction']") public WebElement newTransactionBtn;
	@FindBy(xpath = "//button[contains(text(),'Track Tickets')]") public WebElement tractTicketsBtn;

//	line item grid table
	@FindBy(xpath = "//cpq-table[@aria-label='Line Item Grid']") public WebElement lineItemGridTable;
	@FindBy(xpath = "//input[@type='text']") public WebElement lineItemGridEditBox;
	
//	Billing Information
	@FindBy(xpath = "//input[@value='BCN' and @type='checkbox']//parent::span") public WebElement billingInfoCbx;
	@FindBy(xpath = "//input[@value='Order' and @type='checkbox']//parent::span") public WebElement orderDetailsCbx;
	@FindBy(xpath = "//oj-button[@name='billing_information']") public WebElement billingInformationBtn;
	@FindBy(xpath = "//button[@name='apply']") public WebElement billingLookAapplyBtn;	

//	New BCN Type
	@FindBy(xpath = "//input[@type='search']") public WebElement bcnSearchTxb;
	@FindBy(xpath = "//table[@id='bcnRecordsList']//..//i[1]") public WebElement pickDefaultBCNCbx;
	@FindBy(xpath = "//button[@id='lineItemsSelectionSelectAllBtn']") public WebElement bcnSelectAllBtn;
	@FindBy(xpath = "//button[@id='lineItemsSelectionUpdateAndCloseBtn']") public WebElement bcnUpdatedCloseBtn;
	
//	Generate Proposal
	@FindBy(xpath = "//span[text()='Customer Signature']//parent::a") public WebElement customerSignatureLnk;
	@FindBy(xpath = "//span[text()='Customer Signature']") public WebElement generateProposalLnk;
	@FindBy(xpath = "//div[contains(@id,'select-choice-language')]//child::a") public WebElement proposalLanguageSelectedLst;
	@FindBy(xpath = "//div[contains(@data-oj-containerid,'language')]") public WebElement proposalLanguageLst;
	@FindBy(xpath = "//textarea[contains(@id,'proposalNotes')]") public WebElement proposalNotesTxb;
	@FindBy(xpath = "//span[text()='Generate Proposal']") public WebElement generateProposalBtn;
	@FindBy(xpath = "//div[contains(@id,'showMessageForProposalGeneratedDate_t')]") public WebElement verifyProposalConfElem;
	@FindBy(xpath = "//div[contains(@id,'select-choice-workflow_t')]//child::a") public WebElement selectWorkflowLst;
	@FindBy(xpath = "//div[contains(@data-oj-containerid,'workflow')]") public WebElement workflowLst;
	@FindBy(xpath = "//input[contains(@id,'toRecepient')]") public WebElement toRecepientLst;
	@FindBy(xpath = "//span[text()='Send Proposal']") public WebElement sendProposalBtn;
	@FindBy(xpath = "//span[contains(@id,'file_attachment')]//a") public WebElement fileAttachmentLnk;
	@FindBy(xpath = "//input[@id='nRC_forcontainer']") public WebElement nRcTxb;
	@FindBy(xpath = "//input[@id='mRC_forcontainer']") public WebElement mRcTxb;
	
	
	
//	Save C4C
	@FindBy(xpath = "//span[text()='Save']//parent::div//parent::button") public WebElement saveCPQBtn;
	@FindBy(xpath = "//span[text()='Save']") public WebElement childSaveCPQBtn;	
	@FindBy(xpath = "//descendant::button[@name='save'][3]") public WebElement savePPTBtn;	
	
//	Contact Information
	@FindBy(xpath = "//span[text()='Contact Information']") public WebElement contactInformationLnk;
	@FindBy(xpath = "//table[contains(@aria-describedBy,'additionalQuoteInformation')]") public WebElement additonalInfoTable;
	@FindBy(xpath = "//input[@value='copyContactDetailsFromLegalOrTechnicalOrdeingContact']//parent::span") public WebElement copyContactDetailsRadiobtn;
	@FindBy(xpath = "//input[@value='Ordering Contact']") public WebElement copyOrderingDetailsRadiobtn;
	@FindBy(xpath = "//input[@value='siteContactAEnd']") public WebElement siteContactRadiobtn;
	@FindBy(xpath = "//input[@value='technicalContactAEnd']") public WebElement technicalContactAEndRadiobtn;
	@FindBy(xpath = "//input[@value='electricianContactAEnd']") public WebElement electricianContactAEndRadiobtn;
	@FindBy(xpath = "//input[@value='siteContactBEnd']") public WebElement siteContactBEndRadiobtn;
	@FindBy(xpath = "//input[@value='technicalContactBEnd']") public WebElement technicalContactBEndRadiobtn;
	@FindBy(xpath = "//input[@value='electricianContactBEnd']") public WebElement electricianContactBEndRadiobtn;
	@FindBy(xpath = "//span[text()='Copy Contact']") public WebElement copyContactBtn;
	@FindBy(xpath = "//input[@id='selectJET-1']") public WebElement selectQuote;
	
//	Confirm order
	@FindBy(xpath = "//span[text()='Order']//parent::a") public WebElement OrderLnk;
	@FindBy(xpath = "//div[contains(@id,'choice-quoteAction')]//child::a") public WebElement quoteActionLst;
	@FindBy(xpath = "//ul[contains(@id,'oj-listbox-results-quoteAction')]") public WebElement ActionLst;
	@FindBy(xpath = "//div[contains(@id,'reasonForStatus')]//child::a") public WebElement reasonStatusLst;
	@FindBy(xpath = "//ul[contains(@id,'reasonForStatus')]") public WebElement StatusLst;
	@FindBy(xpath = "//input[contains(@id,'acceptanceDocument')]") public WebElement acceptanceDocumentTxb;
	@FindBy(xpath = "//input[contains(@id,'customerSignedDate')]") public WebElement customerSignedDateElem;
	@FindBy(xpath = "//a[contains(@class,'-selected')]") public WebElement defaultDateElem;
	@FindBy(xpath = "//oj-button[@name='confirm_quote']") public WebElement confirmQuoteBtn;
	@FindBy(xpath = "//span[text()='Create Order']//parent::div//parent::button") public WebElement createOrderBtn;
	@FindBy(xpath = "//oj-switch[contains(@aria-labelledby,'pleaseConfirmThatTheCorrectSignedOrderFormHasBeen')]") public WebElement confirmAttachmentTgleBtn;
	
	
	public CPQ_Objects() {  
		WebDriver driver = DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
		PageFactory.initElements(driver, this);
	}
}
